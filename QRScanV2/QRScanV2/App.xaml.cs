﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace QRScanV2
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage() {BackgroundColor = Color.Yellow });
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            AppCenter.Start("android=f952d410-2a3f-49da-873b-840f5bca7893;" +
                            "uwp=3170858f-87ff-487b-8686-d6af1169c7e5;" +
                            "ios=b1da6787-ddc8-4c74-9ef6-67142442119c;", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
