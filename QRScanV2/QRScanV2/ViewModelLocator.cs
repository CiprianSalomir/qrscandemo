﻿using QRScanV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace QRScanV2
{
    class ViewModelLocator
    {
        private static MainPageViewModel mainPageViewModel;

        public static MainPageViewModel MainPageViewModel { get => mainPageViewModel ?? (mainPageViewModel = new MainPageViewModel()); }
    }
}
