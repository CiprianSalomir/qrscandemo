﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QRScanV2.Models
{
    class PopUpMessage
    {
        public string Message { get; set; }
        public string Result { get; set; }
        public string ButtonText { get; set; }
    }
}
