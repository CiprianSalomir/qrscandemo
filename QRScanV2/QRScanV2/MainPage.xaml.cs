﻿using QRScanV2.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QRScanV2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            HideNavigationBar();
            BindingContext = ViewModelLocator.MainPageViewModel;
            ViewModelLocator.MainPageViewModel.MainPage = this;
        }

        private void HideNavigationBar()
        {
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
