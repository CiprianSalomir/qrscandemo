﻿using QRScanV2.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace QRScanV2.Views
{
    class CustomPopUpWindow : AbsoluteLayout
    {
        private AbsoluteLayout overlay;
        private AbsoluteLayout absolutParent;
        private bool isAnimating;

        private const uint animateTime = 300;

        public CustomPopUpWindow(PopUpMessage message)
        {
            BackgroundColor = Color.White;
            WidthRequest = 300;
            HeightRequest = 200;
            Opacity = 0;

            SetOverlay();

            var messageLabel = new Label()
            {
                Text = message.Message
            };

            var resultLabel = new Label()
            {
                Text = message.Result
            };

            var okButton = new Button()
            {
                Text = message.ButtonText,
                TextColor = Color.White,
                BackgroundColor = Color.OrangeRed,
                WidthRequest = 80,
                HeightRequest = 40,
                BorderWidth = 0,
                CornerRadius = 20
            };

            okButton.Clicked += OkButton_Clicked;

            AbsoluteLayout.SetLayoutBounds(messageLabel, new Rectangle(0.5, 0.2, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            AbsoluteLayout.SetLayoutFlags(messageLabel, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(resultLabel, new Rectangle(0.5, 0.4, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            AbsoluteLayout.SetLayoutFlags(resultLabel, AbsoluteLayoutFlags.PositionProportional);
            AbsoluteLayout.SetLayoutBounds(okButton, new Rectangle(0.5, 0.8, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            AbsoluteLayout.SetLayoutFlags(okButton, AbsoluteLayoutFlags.PositionProportional);

            Children.Add(messageLabel);
            Children.Add(resultLabel);
            Children.Add(okButton);

            AbsoluteLayout.SetLayoutBounds(this, new Rectangle(0.5, 0.5, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
            AbsoluteLayout.SetLayoutFlags(this, AbsoluteLayoutFlags.PositionProportional);
        }

        private void SetOverlay()
        {
            overlay = new AbsoluteLayout()
            {
                BackgroundColor = Color.Black,
                Opacity = 0
            };

            AbsoluteLayout.SetLayoutBounds(overlay, new Rectangle(0.5, 0.5, 1, 1));
            AbsoluteLayout.SetLayoutFlags(overlay, AbsoluteLayoutFlags.All);
        }

        private void AddOverlayInStack()
        {
            if (this.Parent is AbsoluteLayout)
            {
                absolutParent = this.Parent as AbsoluteLayout;
                absolutParent.Children.Add(overlay);
                absolutParent.RaiseChild(this);
            }
        }

        private void RemoveOverlayFromStack()
        {
            absolutParent.Children.Remove(overlay);
        }

        async void OkButton_Clicked(object sender, EventArgs e)
        {
            await AnimateOut();
            ViewModelLocator.MainPageViewModel.OpenURLSafeCommand.Execute(null);
        }

        async public Task AnimateIn()
        {
            if (!isAnimating)
            {
                isAnimating = true;
                AddOverlayInStack();

                await overlay.FadeTo(0.7, animateTime, easing: Easing.Linear);
                this.Opacity = 1;

                isAnimating = false;
            }
        }

        async public Task AnimateOut()
        {
            if (!isAnimating)
            {
                isAnimating = true;

                this.Opacity = 0;
                absolutParent.Children.Remove(this);
                await overlay.FadeTo(0, animateTime, easing: Easing.Linear);
                RemoveOverlayFromStack();

                isAnimating = false;
            }
        }
    }
}
