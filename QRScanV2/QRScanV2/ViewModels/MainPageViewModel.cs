﻿using QRScanV2.Models;
using QRScanV2.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing.Mobile;
using ZXing.Net.Mobile.Forms;

namespace QRScanV2.ViewModels
{
    class MainPageViewModel : BaseViewModel
    {
        private string scanResult;

        public string ScanResult { get { return scanResult; } set { scanResult = value; OnPropertyChanged("ScanResult"); } }
        public MainPage MainPage { get; set; }

        public ZXingScannerPage ScannerPage { get; private set; }
        public MobileBarcodeScanner Scanner { get; private set; }
        public ICommand ScanQRCommand { get; }
        public ICommand OpenNewScanPage { get; }
        public ICommand OpenURLSafeCommand { get; }

        public MainPageViewModel()
        {
            var options = new MobileBarcodeScanningOptions
            {   
                PossibleFormats = new List<ZXing.BarcodeFormat>
                        {
                            ZXing.BarcodeFormat.QR_CODE
                        }
            };

            if(Device.RuntimePlatform == Device.UWP)
            {
                ScannerPage = new ZXingScannerPage(options);
                ScannerPage.OnScanResult += Scanner_OnScanResult;
                ScanQRCommand = new Command(
                    execute: async () =>
                    {
                        await Application.Current.MainPage.Navigation.PushAsync(ScannerPage, true);
                    }
                );
            }
            else if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.iOS)
            {
                Scanner = new MobileBarcodeScanner();
                ScanQRCommand = new Command(
                    execute: async () =>
                    {
                        ZXing.Result result = null;
                        try
                        {
                            result = await Scanner.Scan(options);
                        }
                        catch (Exception e)
                        {
                            Debug.WriteLine(e + " " +  e.Message);
                        }

                        await Task.Delay(200);
                        if (result != null)
                        {
                            await SetResultInUI(result);
                        }
                    }
                );
            }

            OpenNewScanPage = new Command(
                execute: async () =>
                {
                    await Application.Current.MainPage.Navigation.PushAsync(new MainPage() { BackgroundColor = Color.Orange }, true);
                }
            );

            OpenURLSafeCommand = new Command(
                    execute: () =>
                    {
                        try
                        {
                            OpenURLSafe(ScanResult);
                        }
                        catch (Exception ex)
                        {
                            Debug.WriteLine(ex);
                        }
                    }
            );
        }

        private void Scanner_OnScanResult(ZXing.Result result)
        {

            // Stop scanning
            ScannerPage.IsScanning = false;
            // Pop the page
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Application.Current.MainPage.Navigation.PopAsync();
                await Task.Delay(500);
                await SetResultInUI(result);
            });
        }

        async Task SetResultInUI(ZXing.Result result)
        {
            ScanResult = result.Text;
            await ShowPopUpMessage(ScanResult);
        }

        async Task ShowPopUpMessage(string result)
        {
            var popUp = new CustomPopUpWindow(
                new PopUpMessage()
                {
                    Message = "Scanned Barcode",
                    Result = result,
                    ButtonText = "OK"
                }
                );
            (MainPage.Content as AbsoluteLayout).Children.Add(popUp);
            await popUp.AnimateIn();
        }

        private void OpenURLSafe(string result)
        {
            if (result != null)
            {
                if (result.StartsWith("http://", StringComparison.CurrentCulture) ||
                    result.StartsWith("https://", StringComparison.CurrentCulture))
                {
                    OpenURL(result);
                }
                else if (result.EndsWith(".html", StringComparison.CurrentCulture))
                {
                    OpenURL("http://" + result);
                }
            }

            Debug.WriteLine("Scanned Barcode: " + result);
        }

        private static void OpenURL(string text)
        {
            Device.OpenUri(new Uri(text));
        }
    }
}